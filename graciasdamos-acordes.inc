\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	R4

	% gracias damos
	e2 b2
	cis2:m b2
	e2 fis2
	b2:sus4 b2
	e2. fis4:m
	gis1:m
	a2 b2
	e2 e2:7

	% esta hostia sera...
	a2 a2:m e2 e2:7
	a2 a2:m e2 e2:7
	a2 fis2:7 b2:7 b2:7

	% gracias damos
	e2 b2
	cis2:m b2
	e2 fis2
	b2:sus4 b2
	e2. fis4:m
	gis1:m
	a2 b2
	e2 e2:7

	% esta hostia sera...
	a2 a2:m e2 e2:7
	a2 a2:m e2 e2:7
	a2 fis2:7 b2:7 b2:7

	% gracias damos
	e2 b2
	cis2:m b2
	e2 fis2
	b2:sus4 b2
	e2. fis4:m
	gis1:m
	a2 b2

	% finale
	e2 e2:7
	a2 a2:m
	e2
	}
