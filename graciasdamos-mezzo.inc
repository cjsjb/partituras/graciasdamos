\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key e \major

		gis 8. a 16  |
		b 4 e' 2 fis' 8. gis' 16  |
		e' 2. gis' 8. fis' 16  |
		e' 4 e' 2 e' 8. e' 16  |
%% 5
		dis' 2. b 8. b 16  |
		e' 4 e' 2 fis' 4  |
		gis' 2. e' 8. e' 16  |
		cis' 4 e' ( a' 8 gis' ) fis' 4  |
		e' 2 r4 e' 8. e' 16  |
%% 10
		cis' 4 cis' ( c' ) c'  |
		b 2 ( d' 4 ) d' 8. d' 16  |
		cis' 4 cis' 4. c' 16. c' c' c'  |
		b 2 ( d' 4 ) d' 8. d' 16  |
		cis' 4 cis' 4. cis' 16. cis' cis' cis'  |
%% 15
		dis' 2 r4 gis 8. a 16  |
		b 4 e' 2 fis' 8. gis' 16  |
		e' 2. gis' 8. fis' 16  |
		e' 4 e' 2 e' 8. e' 16  |
		dis' 2. b 8. b 16  |
%% 20
		e' 4 e' 2 fis' 4  |
		gis' 2. e' 8. e' 16  |
		cis' 4 e' ( a' 8 gis' ) fis' 4  |
		e' 2 r4 e' 8. e' 16  |
		cis' 4 cis' ( c' ) c'  |
%% 25
		b 2 ( d' 4 ) d' 8. d' 16  |
		cis' 4 cis' 4. c' 16. c' c' c'  |
		b 2 ( d' 4 ) d' 8. d' 16  |
		cis' 4 cis' 4. cis' 16. cis' cis' cis'  |
		dis' 2 r4 gis 8. a 16  |
%% 30
		b 4 e' 2 fis' 8. gis' 16  |
		e' 2. gis' 8. fis' 16  |
		e' 4 e' 2 e' 8. e' 16  |
		dis' 2. b 8. b 16  |
		e' 4 e' 2 fis' 4  |
%% 35
		gis' 2. e' 8. e' 16  |
		cis' 4 e' ( a' 8 gis' ) fis' 4  |
		e' 2 ( d'  |
		cis' 2 c'  |
		b 1 )  |
%% 40
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "mezzo" {
		Gra -- cias da -- mos al Se -- ñor
		por dar -- nos su ben -- di -- ción,
		por sen -- tir -- nos con él
		es -- te dí -- a __ "de a" -- mor.

		Es -- ta hos -- tia __ se -- rá __
		la que guar -- de en mi co -- ra -- zón. __
		Pu -- ri -- fi -- ca "mi al" -- ma pa -- ra ti.

		Gra -- cias da -- mos al Se -- ñor
		por la vi -- da del pas -- tor.
		A -- le -- jan -- dro se -- rá
		nues -- tro guí -- a __ "de a" -- mor.

		Es -- ta hos -- tia __ se -- rá __
		la que guar -- de en mi co -- ra -- zón. __
		Pu -- ri -- fi -- ca "mi al" -- ma pa -- ra ti.

		Gra -- cias da -- mos al Se -- ñor
		por la vi -- da del pas -- tor.
		A -- le -- jan -- dro se -- rá
		nues -- tro guí -- a __ "de a" -- mor. __
	}

>>
