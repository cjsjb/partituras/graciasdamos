\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key e \major

		gis, 8. a, 16  |
		b, 4 e 2 fis 8. gis 16  |
		e 2. gis 8. fis 16  |
		e 4 gis 2 b 8. b 16  |
%% 5
		fis 2. b, 8. b, 16  |
		gis 4 gis 2 a 4  |
		b 2. gis 8. e 16  |
		cis' 4 b 2 a 4  |
		gis 2 r4 e 8. e 16  |
%% 10
		e 4 e 2 fis 4  |
		e 2 r4 e 8. e 16  |
		e 4 e 4. e 16. fis e fis  |
		e 2. e 8. e 16  |
		e 4 e 4. e 16. e dis e  |
%% 15
		fis 2 r4 gis, 8. a, 16  |
		b, 4 e 2 fis 8. gis 16  |
		e 2. gis 8. fis 16  |
		e 4 gis 2 b 8. b 16  |
		fis 2. b, 8. b, 16  |
%% 20
		gis 4 gis 2 a 4  |
		b 2. gis 8. e 16  |
		cis' 4 b 2 a 4  |
		gis 2 r4 e 8. e 16  |
		e 4 e 2 fis 4  |
%% 25
		e 2 r4 e 8. e 16  |
		e 4 e 4. e 16. fis e fis  |
		e 2. e 8. e 16  |
		e 4 e 4. e 16. e dis e  |
		fis 2 r4 gis, 8. a, 16  |
%% 30
		b, 4 e 2 fis 8. gis 16  |
		e 2. gis 8. fis 16  |
		e 4 gis 2 b 8. b 16  |
		fis 2. b, 8. b, 16  |
		gis 4 gis 2 a 4  |
%% 35
		b 2. gis 8. e 16  |
		cis' 4 b 2 a 4  |
		gis 1 (  |
		a 1  |
		b 1 )  |
%% 40
		R1  |
		\bar "|."
	}

	\new Lyrics\lyricsto "tenor" {
		Gra -- cias da -- mos al Se -- ñor
		por dar -- nos su ben -- di -- ción,
		por sen -- tir -- nos con él
		es -- te dí -- a "de a" -- mor.

		Es -- ta hos -- tia se -- rá
		la que guar -- de en mi co -- ra -- zón.
		Pu -- ri -- fi -- ca "mi al" -- ma pa -- ra ti.

		Gra -- cias da -- mos al Se -- ñor
		por la vi -- da del pas -- tor.
		A -- le -- jan -- dro se -- rá
		nues -- tro guí -- a "de a" -- mor.

		Es -- ta hos -- tia se -- rá
		la que guar -- de en mi co -- ra -- zón.
		Pu -- ri -- fi -- ca "mi al" -- ma pa -- ra ti.

		Gra -- cias da -- mos al Se -- ñor
		por la vi -- da del pas -- tor.
		A -- le -- jan -- dro se -- rá
		nues -- tro guí -- a "de a" -- mor. __
	}

>>
